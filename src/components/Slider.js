import React, { PureComponent } from 'react';
import Carousel from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';

export default class Slider extends PureComponent {
  render() {
    return (
      <div className="container">
        <Carousel 
          slidesPerPage={3}
          clickToChange
          infinite
          dots
          breakpoints={{
            576: {
              slidesPerPage: 1,
              arrows: false
            },
            768: {
              slidesPerPage: 2,
              arrows: false
            }
          }}
        >
          
          <div className="card pa-30 brd-none">
            <img
              className="card-img-top img-fluid"
              src="/assets/img/image-1.jpg"
              alt="Card cap"
            />
            <div className="card-body py-15">
              <h5 className="card-title h5">Save Time on What Matters</h5>
              <p className="card-text">
                Spend less time paying nills, and sign up for auto pay.
              </p>
            </div>
          </div>
          <div className="card pa-30 brd-none">
            <img
              className="card-img-top img-fluid"
              src="/assets/img/image-2.jpg"
              alt="Card cap"
            />
            <div className="card-body py-15">
              <h5 className="card-title h5">Paying in Cash?</h5>
              <p className="card-text">
                Pay for free at any of hundreds of convenient and secure Western Union.
              </p>
            </div>
          </div>
          <div className="card pa-30 brd-none">
            <img
              className="card-img-top img-fluid"
              src="/assets/img/image-3.jpg"
              alt="Card cap"
            />
            <div className="card-body py-15">
              <h5 className="card-title h5">Don't get Scammed</h5>
              <p className="card-text">
                PGE Employees will never ask you for payment information over the phone.
              </p>
            </div>
          </div>
          <div className="card pa-30 brd-none">
            <img
              className="card-img-top img-fluid"
              src="/assets/img/image-1.jpg"
              alt="Card cap"
            />
            <div className="card-body py-15">
              <h5 className="card-title h5">Paying Check</h5>
              <p className="card-text">
                PGE Employees will never ask you for payment information over the phone.
              </p>
            </div>
          </div>
        </Carousel>
      </div>
    )
  }
}
