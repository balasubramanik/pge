import React, { PureComponent } from 'react';
import { 
  Accordion, 
  Card 
} from 'react-bootstrap';
import CustomToggle from './CustomToggle';

export default class Help extends PureComponent {
  render() {
    return (
      <section className="bg-light">
        <div className="container">
          {/* <!-- Heading --> */}
            <div>
              <div className="d-inline-block"></div>
              <h2 className="h4 mb-20">Help</h2>
            </div>
          {/* <!-- End Heading --> */}
          <Accordion>
            <Card className="brd-none shadow mb-10">
              <CustomToggle eventKey="0">Help Center</CustomToggle>
              <Accordion.Collapse eventKey="0">
                <Card.Body className="color-gray-dark pa-30 medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card className="brd-none shadow mb-10">
              <CustomToggle eventKey="1">Bill Payment Assistance</CustomToggle>
              <Accordion.Collapse eventKey="1">
                <Card.Body className="color-gray-dark pa-30 medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card className="brd-none shadow mb-10">
              <CustomToggle eventKey="2">Understanding My Bill</CustomToggle>
              <Accordion.Collapse eventKey="2">
                <Card.Body className="color-gray-dark pa-30 medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card className="brd-none shadow mb-10">
              <CustomToggle eventKey="3">Certified Contractors</CustomToggle>
              <Accordion.Collapse eventKey="3">
                <Card.Body className="color-gray-dark pa-30 medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card className="brd-none shadow mb-10">
              <CustomToggle eventKey="4">Contact Us</CustomToggle>
              <Accordion.Collapse eventKey="4">
                <Card.Body className="color-gray-dark pa-30 medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        </div>
      </section>
    )
  }
}
