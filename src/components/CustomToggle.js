import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, useAccordionToggle } from "react-bootstrap";

function CustomToggle({ children, eventKey }) {
  const [toggle, setToggle] = useState(false);
  const decoratedOnClick = useAccordionToggle(eventKey, () => {
    setToggle(!toggle);
  });

  return (
    <Button
      variant="link"
      className="collapsed d-flex justify-content-between color-main rounded px-30 py-20"
      onClick={decoratedOnClick}
    >
      {children}
      <span className="accordion-control-icon color-primary float-right">
        <FontAwesomeIcon
          icon="angle-up"
          className={toggle ? "show" : "collapse"}
        />
        <FontAwesomeIcon
          icon="angle-down"
          className={toggle ? "collapse" : "show"}
        />
      </span>
    </Button>
  );
}

export default CustomToggle;
